const api_url = 'http://localhost:8000/api/v1/colors';

let order = {'by': 'name', 'direction' : 'asc'}

function getColors() {
  $.ajax({
    url: api_url + '?order_by='+order.by+'&order_by_direction='+order.direction,
    success: function(data) {
        $("#colors-list").empty();
        $.each(data.colors, function(index, color) {
            $("#colors-list").append(
                '<tr>'
            +     '<td>' + color.name + '</td>'
            +     '<td>' + color.hex_code + '</td>'
            +     '<td><div class="circle" style="background-color: #'+color.hex_code+'"></div></td>'    
            +     '<td><div class="remove" onclick="removeColor('+color.id+');">X</div></td>'    
            + '</div>'
            );    
        });
    }, error: function (data) {
        printErrorMessage(data.responseJSON)
    }
  });
}


$("#add-color").submit(function(e){
    e.preventDefault();
    $.ajax({
        url: api_url,
        type: 'POST',
        data: { name: $('#add-color #name').val(), hex_code : $('#add-color #hex_code').val()} ,
        success: function(data) {
            printSuccessMessage(data.message);
            getColors();
            $("#add-color")[0].reset();
        }, error: function (data) {
            printErrorMessage(data.responseJSON)
        }
    });
});


function removeColor(id_color) {
    $.ajax({
        url: api_url + '/' + id_color,
        type: 'DELETE',
        success: function(data) {
            printSuccessMessage(data.message);
            getColors();
        }, error: function (data) {
            printErrorMessage(data.responseJSON)
        }
    });   
}


$(".order_by").click(function(){
    order.by = $(this).data('order_by');
    order.direction =  $(this).data('order_by_direction');
    getColors();
    $('.order_by').removeClass('selected');
    $(this).addClass('selected')
});


function printErrorMessage(messages) {
    $("#api-response").html('<div class="alert alert-danger" role="alert">');
    $.each(messages, function(index, message) {
        $(".alert-danger").append('<p>'+message+'</p>');
    });
    $("#api-response").append('</div>');
}


function printSuccessMessage(message) {
    $("#api-response").html(
        '<div class="alert alert-success" role="alert">'
        +  message
        +'</div>'
    );
}


getColors();