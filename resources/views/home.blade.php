<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Frontend Colors</title>
  <meta name="description" content="This is the frontend to consume the API colors">
  <meta name="author" content="Javier Prats">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="{{ url('/css/main.css') }}">
</head>

<body>
    <main>
      <div class="container pt-5">
        <h1>Colors</h1>
        <div id="api-response">

        </div>
        <table class="table mt-4 mb-5">
          <thead>
            <tr>
              <th>
                Name
                <span data-order_by="name" data-order_by_direction="asc" class="order_by selected">&#8593;</span>
                <span data-order_by="name" data-order_by_direction="desc" class="order_by">&#8595;</span>
              </th>
              <th>
                Hex Code
                <span data-order_by="hex_code" data-order_by_direction="asc" class="order_by">&#8593;</span>
                <span data-order_by="hex_code" data-order_by_direction="desc" class="order_by">&#8595;</span>
              </th>
              <th>Sample</th>
              <th>Remove</th>
            </tr>
          </thead>
          <tbody  id="colors-list"></tbody>
        </table>

        <hr>
        
        <h4 class="mt25">Add new color</h4>
        <form class="form-inline" id="add-color" autocomplete="off">
          <input id="name" placeholder="Name" class="form-control mr-2" type="text" maxlength="60" name="name">
          <input id="hex_code" placeholder="Hex color" class="form-control mx-2" type="text" maxlength="6" name="hex_code">   
          <input type="submit" class="btn btn-primary mx-2" value="Submit">   
        </form>
      </div>
    </main>
    <script  src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
    crossorigin="anonymous"></script>
    <script src="{{ url('/js/scripts.js') }}"></script>
</body>
</html>