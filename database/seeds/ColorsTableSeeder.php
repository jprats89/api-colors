<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colors')->insert([
            [
                'name' => 'Gold',
                'hex_code' => 'FFD700',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Salmon',
                'hex_code' => 'FA8072',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Limegreen',
                'hex_code' => '32CD32',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Aqua',
                'hex_code' => '00FFFF',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Navy',
                'hex_code' => '000080',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Violet',
                'hex_code' => 'EE82EE',
                'created_at' => Carbon::now(),
            ],
        ]);
    }
}
