<?php

class GetColorsTest extends TestCase
{
    private $route = '/api/v1/colors';

    public function testOkStatus()
    {
        $this->get($this->route);
        $this->seeStatusCode(200);
    }

    public function testResponseStructure()
    {
        $this->get($this->route);
        $this->seeJsonStructure([
            'colors' => ['*' =>
                [
                    'id',
                    'name',
                    'hex_code'
                ]
            ]
        ]);
    }

    public function testFirstDatabaseRecordReturned()
    {
        $this->get($this->route)->seeJson([
            'name' => DB::table('colors')->first()->name,
        ]);
    }


    public function testLastDatabaseRecordReturned()
    {
        $this->get($this->route)->seeJson([
            'name' => DB::table('colors')->orderBy('id', 'DESC')->first()->name,
        ]);
    }

    public function testItemsNumberReturnedMatchDatabaseRecordsNumber()
    {
        $response = $this->get($this->route)->response->decodeResponseJson();
        $this->assertEquals(count($response['colors']), DB::table('colors')->count());
    }

    public function testItemsAreReturnedInTheSpecifiedOrder()
    {
        $response = $this->get($this->route.'?order_by_direction=desc')->response->decodeResponseJson();
        $this->assertEquals($response['colors'][0]['id'], DB::table('colors')->count());
    }

    public function testOrderByParamMustExistInDatabase()
    {
        $this->get($this->route.'?order_by=rgb')
            ->seeJson(['order_by' => ["The selected order by is invalid."]]);
    }
}
