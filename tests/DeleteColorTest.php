<?php

class DeleteColorTest extends TestCase
{
    private $route = '/api/v1/colors';

    public function testOkStatus()
    {
        $this->json('DELETE', $this->route . '/1');
        $this->seeStatusCode(200);
    }

    public function testExistingRecordCanBeDeleted()
    {
        $this->seeInDatabase('colors', ['id' => 1]);
        $this->json('DELETE', $this->route . '/1')->seeJson(['message' => 'Color deleted successfully!']);
        $this->notSeeInDatabase('colors', ['id' => 1]);        
    }
}
