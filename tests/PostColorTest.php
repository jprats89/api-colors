<?php

class PostColorTest extends TestCase
{
    private $route = '/api/v1/colors';
    
    public function testOkStatus()
    {
        $olive = ['name' => 'Olive', 'hex_code' => '808000'];
        $this->json('POST', $this->route, $olive);
        $this->seeStatusCode(200);
    }

    public function testColorCanBeCreated()
    {
        $olive = ['name' => 'Olive', 'hex_code' => '808000'];
        $black = ['name' => 'Black', 'hex_code' => '000'];

        $this->notSeeInDatabase('colors', $olive);
        $this->notSeeInDatabase('colors', $black);

        $this->json('POST', $this->route, $olive)
            ->seeJson(['message' => 'Color created successfully!']);
        $this->json('POST', $this->route, $black)
            ->seeJson(['message' => 'Color created successfully!']);

        $this->seeInDatabase('colors', $olive);
        $this->seeInDatabase('colors', $black);
    }

    public function testColorsNameMustBeUnique()
    {
        $olive1 = ['name' => 'Olive', 'hex_code' => '808000'];
        $olive2 = ['name' => 'Olive', 'hex_code' => '000000'];
        
        $this->json('POST', $this->route, $olive1)
            ->seeJson(['message' => 'Color created successfully!']);     
        $this->json('POST', $this->route, $olive2)->seeJson(['name' => ["The name has already been taken."]]);

        $this->seeInDatabase('colors', $olive1);
        $this->notSeeInDatabase('colors', $olive2);
    }

    public function testHexCodeMustBeUnique()
    {
        $olive = ['name' => 'Olive', 'hex_code' => '808000'];
        $dark_olive = ['name' => 'DarkOlive', 'hex_code' => '808000'];
        
        $this->json('POST', $this->route, $olive)
            ->seeJson(['message' => 'Color created successfully!']);
        $this->json('POST', $this->route, $dark_olive)->seeJson(['hex_code' => ["The hex code has already been taken."]]);

        $this->seeInDatabase('colors', $olive);
        $this->notSeeInDatabase('colors', $dark_olive);
    }

    public function testColorNameCannotBeNull()
    {
        $no_name_color = ['name' => '', 'hex_code' => '808000'];
        
        $this->json('POST', $this->route, $no_name_color)->seeJson(['name' => ["The name field is required."]]);        
        $this->notSeeInDatabase('colors', $no_name_color);
    }

    public function testColorNameLengthCannotBeGreaterThanSixty()
    {
        $lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
        ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
        nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit...";
        
        $overlength_name_color = ['name' => $lorem_ipsum, 'hex_code' => '808000'];
        
        $this->json('POST', $this->route, $overlength_name_color)->seeJson([
            'name' => ["The name may not be greater than 60 characters."],
         ]);

        $this->notSeeInDatabase('colors', $overlength_name_color);
    }

    public function testColorHexCodeMustHaveRightLength()
    {
        $wrong_length_code_color = ['name' => 'olive', 'hex_code' => '8080'];
        $this->notSeeInDatabase('colors', $wrong_length_code_color);
        $this->json('POST', $this->route, $wrong_length_code_color)->seeJson([
            'hex_code' => ["The hex code format is invalid."],
        ]);
        $this->notSeeInDatabase('colors', $wrong_length_code_color);
    }
   

    public function testColorHexCodeMustBeWellFormed()
    {
        $wrong_hex_code_color = ['name' => 'olive', 'hex_code' => '8Z8Z8Z'];
        $this->notSeeInDatabase('colors', $wrong_hex_code_color);
        $this->json('POST', $this->route, $wrong_hex_code_color)->seeJson([
            'hex_code' => ["The hex code format is invalid."],
        ]);
        $this->notSeeInDatabase('colors', $wrong_hex_code_color);
    }
}
