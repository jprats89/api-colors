<?php

namespace App\Http\Controllers;
use App\Color;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ColorController extends Controller
{
    public function index(Request $request)
    {
        $this->validate($request, [
            'order_by' => [Rule::in(['id', 'name', 'hex_code'])],
            'order_by_direction' => [Rule::in(['asc', 'desc'])]
        ]);
        
        $order_by = ($request->input('order_by')) ? $request->input('order_by') : 'id';
        $order_by_direction = ($request->input('order_by_direction')) ? $request->input('order_by_direction') : 'ASC';

        $colors = Color::select('id', 'name', 'hex_code')->orderBy($order_by, $order_by_direction)->get();
        return  response()->json(['colors' => $colors]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:colors|max:60',
            'hex_code' => ['required', 'unique:colors', 'regex:/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/']
        ]);
        
        $color = Color::create($request->all());
        return response()->json(['message' => 'Color created successfully!', 'color_id' => $color->id]);
    }

    public function destroy($id_color)
    {
        Color::destroy($id_color);
        return response()->json(['message' => 'Color deleted successfully!']);
    }

}
